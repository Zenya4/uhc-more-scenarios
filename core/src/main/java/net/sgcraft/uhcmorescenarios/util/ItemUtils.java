package net.sgcraft.uhcmorescenarios.util;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemUtils {
    public static ItemStack addLore(ItemStack item, String str) {
        str = ChatColor.translateAlternateColorCodes('&', str);

        if(item == null || item.getType().equals(Material.AIR)) {
            return item;
        }

        ItemMeta meta = item.getItemMeta();
        List<String> lores = meta.getLore();

        if(lores == null || lores.size() == 0) {
            lores = new ArrayList<String>();
        }

        lores.add(str);
        meta.setLore(lores);
        item.setItemMeta(meta);

        return item;
    }

    public static ItemStack setLores(ItemStack item, ArrayList<String> lores) {
        ArrayList<String> newLores = new ArrayList<String>();
        for(String str : lores) {
            str = ChatColor.translateAlternateColorCodes('&', str);
            newLores.add(str);
        }

        if(item == null || item.getType().equals(Material.AIR)) {
            return item;
        }

        ItemMeta meta = item.getItemMeta();
        meta.setLore(lores);
        item.setItemMeta(meta);

        return item;
    }

    public static ItemStack removeLore(ItemStack item, String str) {
        str = ChatColor.translateAlternateColorCodes('&', str);

        if(item == null || item.getType().equals(Material.AIR)) {
            return item;
        }

        ItemMeta meta = item.getItemMeta();
        List<String> lores = meta.getLore();

        if(lores == null || lores.size() == 0) {
            return item;
        }

        String loreToRemove = null;

        for (String lore : lores) {
            if (lore.contains(str)) {
                loreToRemove = lore;
            }
        }

        if (!(loreToRemove == null)) {
            lores.remove(loreToRemove);
            meta.setLore(lores);
            item.setItemMeta(meta);
        }
        return item;
    }

    public static ItemStack setName(ItemStack item, String str) {
        str = ChatColor.translateAlternateColorCodes('&', str);

        if (item == null || item.getType().equals(Material.AIR)) {
            return item;
        }

        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(str);
        item.setItemMeta(meta);
        return item;
    }
}

