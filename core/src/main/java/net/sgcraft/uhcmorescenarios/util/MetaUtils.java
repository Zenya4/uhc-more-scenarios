package net.sgcraft.uhcmorescenarios.util;

import net.sgcraft.uhcmorescenarios.UHCMoreScenarios;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

public class MetaUtils {
    public static boolean hasMeta(Entity entity, String meta) {
        return entity.hasMetadata(meta);
    }

    public static boolean hasMeta(Player player, String meta) {
        return player.hasMetadata(meta);
    }

    public static void setMeta(Entity entity, String meta, Object value) {
        entity.setMetadata(meta, new FixedMetadataValue(UHCMoreScenarios.getInstance(), value));
    }

    public static void setMeta(Player player, String meta, Object value) {
        player.setMetadata(meta, new FixedMetadataValue(UHCMoreScenarios.getInstance(), value));
    }

    public static void clearMeta(Entity entity, String meta) {
        if(hasMeta(entity, meta)) {
            entity.removeMetadata(meta, UHCMoreScenarios.getInstance());
        }
    }

    public static void clearMeta(Player player, String meta) {
        if(hasMeta(player, meta)) {
            player.removeMetadata(meta, UHCMoreScenarios.getInstance());
        }
    }

    public static String getMetaValue(Entity entity, String meta) {
        if(!(hasMeta(entity, meta)) || entity.getMetadata(meta).size() == 0) return "";
        return entity.getMetadata(meta).get(0).asString();
    }

    public static String getMetaValue(Player player, String meta) {
        if(!(hasMeta(player, meta)) || player.getMetadata(meta).size() == 0) return "";
        return player.getMetadata(meta).get(0).asString();
    }
}

