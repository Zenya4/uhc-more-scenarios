package net.sgcraft.uhcmorescenarios.command;

import net.sgcraft.uhcmorescenarios.gui.MoreScenariosGUI;
import net.sgcraft.uhcmorescenarios.util.ChatUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MoreScenariosCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if(!(sender instanceof Player)) {
            ChatUtils.sendMessage(sender, "&4Only players are allowed to use this command");
            return true;
        }

        Player player = (Player) sender;
        MoreScenariosGUI.getInstance().openInventory(player);
        return true;
    }
}
