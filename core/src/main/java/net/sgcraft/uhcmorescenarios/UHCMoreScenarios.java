package net.sgcraft.uhcmorescenarios;

import net.sgcraft.uhcmorescenarios.command.MoreScenariosCommand;
import net.sgcraft.uhcmorescenarios.event.Listeners;
import net.sgcraft.uhcmorescenarios.file.ConfigManager;
import org.bukkit.plugin.java.JavaPlugin;

public class UHCMoreScenarios extends JavaPlugin {
    private static UHCMoreScenarios instance;

    public void onEnable() {
        instance = this;

        //Initialise config
        ConfigManager.getInstance();

        //Register listeners
        this.getServer().getPluginManager().registerEvents(new Listeners(), this);

        //Register commands
        this.getCommand("morescenarios").setExecutor(new MoreScenariosCommand());
    }

    public void onDisable() {

    }

    public static UHCMoreScenarios getInstance() {
        return instance;
    }
}
