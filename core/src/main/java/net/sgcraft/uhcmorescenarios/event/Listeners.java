package net.sgcraft.uhcmorescenarios.event;

import net.sgcraft.uhcmorescenarios.gui.MoreScenariosGUI;
import net.sgcraft.uhcmorescenarios.scenarios.EventType;
import net.sgcraft.uhcmorescenarios.scenarios.ScenarioManager;
import net.sgcraft.uhcmorescenarios.scenarios.UHCScenario;
import net.sgcraft.uhcmorescenarios.util.ChatUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class Listeners implements Listener {
    @EventHandler
    public void onInventoryClickEvent(InventoryClickEvent event) {
        MoreScenariosGUI scenariosGUI = MoreScenariosGUI.getInstance();
        if (event.getClickedInventory() != null && event.getClickedInventory().getHolder() != null && event.getClickedInventory().getHolder().equals(scenariosGUI)) {
            Player player = (Player) event.getWhoClicked();
            ItemStack clickedItem = event.getCurrentItem();
            if (clickedItem == null || clickedItem.getType() == Material.AIR) {
                return;
            }

            scenariosGUI.initItems();
            if (event.getAction().equals(InventoryAction.PICKUP_ALL)) {
                UHCScenario scenario = ScenarioManager.getInstance().getScenario(clickedItem);
                if (scenario != null) {
                    if(player.hasPermission("uhcmorescenarios.use")) {
                        if (scenario.isEnabled()) {
                            scenario.setEnabled(false);
                            MoreScenariosGUI.getInstance().openInventory(player);
                            ChatUtils.sendMessage(player, "&7Scenario " + scenario.getDisplayName() + " &7has been &cdisabled");
                        } else {
                            scenario.setEnabled(true);
                            MoreScenariosGUI.getInstance().openInventory(player);
                            ChatUtils.sendMessage(player, "&7Scenario " + scenario.getDisplayName() + " &7has been &aenabled");
                        }
                    }
                }
            }
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onProjectileLaunchEvent(ProjectileLaunchEvent event) {
        ScenarioHandler.handle(event, EventType.PROJECTILELAUNCHEVENT);
    }

    @EventHandler
    public void onProjectileHitEvent(ProjectileHitEvent event) {
        ScenarioHandler.handle(event, EventType.PROJECTILEHITEVENT);
    }

    @EventHandler
    public void onE(EntityDamageByEntityEvent event) {
        ScenarioHandler.handle(event, EventType.ENTITYDAMAGEBYENTITYEVENT);
    }
}
