package net.sgcraft.uhcmorescenarios.event;

import net.sgcraft.uhcmorescenarios.scenarios.EventType;
import net.sgcraft.uhcmorescenarios.scenarios.Scenario;
import net.sgcraft.uhcmorescenarios.scenarios.ScenarioManager;
import net.sgcraft.uhcmorescenarios.scenarios.UHCScenario;
import org.bukkit.event.Event;

public class ScenarioHandler {
    private static ScenarioManager scenarioManager = ScenarioManager.getInstance();

    public static void handle(Event e, EventType t) {
        for(Scenario key : scenarioManager.getKeys()) {
            UHCScenario scenario = scenarioManager.getScenario(key);
            if(!scenario.isEnabled()) continue;

            try {
                if(scenario.getEvents().contains(EventType.valueOf(e.getEventName().toUpperCase()))) {
                    scenario.onTrigger(e, t);
                }
            } catch(Exception exc) {
                //Enum not found, do nothing
            }
        }
    }
}
