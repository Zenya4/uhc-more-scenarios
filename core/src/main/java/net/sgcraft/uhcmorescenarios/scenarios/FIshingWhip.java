package net.sgcraft.uhcmorescenarios.scenarios;

import net.sgcraft.uhcmorescenarios.util.ChatUtils;
import net.sgcraft.uhcmorescenarios.util.ItemUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class FIshingWhip implements UHCScenario {
    boolean enabled;

    public FIshingWhip() {
        this.enabled = false;
    }

    @Override
    public void onTrigger(Event event, EventType name) {
        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        if(!(e.getDamager().getType().equals(EntityType.FISHING_HOOK)) || !(e.getEntity() instanceof Player)) return;

        e.getEntity().setVelocity(new Vector(0, 2, 0));
    }

    @Override
    public Scenario getKey() {
        return Scenario.FISHING_WHIP;
    }

    @Override
    public ArrayList<EventType> getEvents() {
        ArrayList<EventType> types = new ArrayList<>();
        types.add(EventType.ENTITYDAMAGEBYENTITYEVENT);
        return types;
    }

    @Override
    public Material getMaterial() {
        return Material.FISHING_ROD;
    }

    @Override
    public String getDisplayName() {
        return "&5Fishing Whip";
    }

    @Override
    public ArrayList<String> getDescription() {
        ArrayList<String> lores = new ArrayList<String>();
        lores.add(ChatUtils.parseMessage("&fYour fishing rod now launches players into the air"));
        return lores;
    }

    //These methods below should be the same for all scenarios
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public ItemStack getGUIItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemUtils.setName(item, getDisplayName());
        ItemUtils.setLores(item, getDescription());
        if(this.enabled) {
            item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
        } else {
            item.removeEnchantment(Enchantment.DURABILITY);
        }
        return item;
    }
}
