package net.sgcraft.uhcmorescenarios.scenarios;

import net.sgcraft.uhcmorescenarios.UHCMoreScenarios;
import net.sgcraft.uhcmorescenarios.util.ChatUtils;
import net.sgcraft.uhcmorescenarios.util.ItemUtils;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.event.Event;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Random;

public class ElectricSnowballs implements UHCScenario {
    boolean enabled;

    public ElectricSnowballs() {
        this.enabled = false;
    }

    @Override
    public void onTrigger(Event event, EventType name) {
        //To support multiple event listeners in your scenario, use the switch statement to catch different listeners and execute code accordingly
        switch(name) {
            //Code will run if event is ProjectileLaunchEvent
            case PROJECTILELAUNCHEVENT: {
                ProjectileLaunchEvent e = (ProjectileLaunchEvent) event;
                if(!e.getEntityType().equals(EntityType.SNOWBALL)) return;

                new BukkitRunnable()
                {
                    public void run()
                    {
                        if (e.getEntity().isDead()) {
                            cancel();
                        }
                        Location loc = e.getEntity().getLocation();
                        e.getEntity().getWorld().strikeLightning(e.getEntity().getLocation());
                    }
                }.runTaskTimer(UHCMoreScenarios.getInstance(), 5L, 3L);
                break;
            }

            //Code will run if event is ProjectileHitEvent
            case PROJECTILEHITEVENT: {
                ProjectileHitEvent e = (ProjectileHitEvent) event;
                if(!e.getEntityType().equals(EntityType.SNOWBALL)) return;

                Location loc = e.getEntity().getLocation();
                World world = loc.getWorld();
                Firework firework = (Firework) world.spawnEntity(loc, EntityType.FIREWORK);
                FireworkMeta fireworkMeta = firework.getFireworkMeta();

                FireworkEffect.Type type = null;
                Random randObj = new Random();

                int _type = randObj.nextInt(3) + 1;
                int colors = randObj.nextInt(3) + 1;
                Color c1 = null;
                Color c2 = null;

                switch (_type) {
                    case 1:
                        type = FireworkEffect.Type.BALL;
                        break;
                    case 2:
                        type = FireworkEffect.Type.BALL_LARGE;
                        break;
                    case 3:
                        type = FireworkEffect.Type.BURST;
                        break;
                }

                switch (colors) {
                    case 1:
                        c1 = Color.RED;
                        c2 = Color.ORANGE;
                        break;
                    case 2:
                        c1 = Color.BLUE;
                        c2 = Color.TEAL;
                        break;
                    case 3:
                        c1 = Color.GREEN;
                        c2 = Color.LIME;
                        break;
                }

                FireworkEffect effect = FireworkEffect.builder().flicker(false).withColor(c1).withFade(c2).trail(true).with(type).trail(false).build();
                world.createExplosion(loc, 0f);
                fireworkMeta.addEffect(effect);
                firework.setFireworkMeta(fireworkMeta);

                // Boom
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        firework.detonate();
                    }
                }.runTaskLater(UHCMoreScenarios.getInstance(), 1l);
                break;
            }
        }
    }

    @Override
    public Scenario getKey() {
        return Scenario.ELECTRIC_SNOWBALLS;
    }

    @Override
    public ArrayList<EventType> getEvents() {
        ArrayList<EventType> types = new ArrayList<>();
        types.add(EventType.PROJECTILELAUNCHEVENT);
        types.add(EventType.PROJECTILEHITEVENT);
        return types;
    }

    @Override
    public Material getMaterial() {
        return Material.SNOW_BALL;
    }

    @Override
    public String getDisplayName() {
        return "&bElectric Snowballs";
    }

    @Override
    public ArrayList<String> getDescription() {
        ArrayList<String> lores = new ArrayList<String>();
        lores.add(ChatUtils.parseMessage("&fSnowballs will summon a streak of lightning bolts"));
        lores.add(ChatUtils.parseMessage("&fAnd explode upon contact"));
        return lores;
    }

    //These methods below should be the same for all scenarios
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public ItemStack getGUIItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemUtils.setName(item, getDisplayName());
        ItemUtils.setLores(item, getDescription());
        if(this.enabled) {
            item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
        } else {
            item.removeEnchantment(Enchantment.DURABILITY);
        }
        return item;
    }


}
