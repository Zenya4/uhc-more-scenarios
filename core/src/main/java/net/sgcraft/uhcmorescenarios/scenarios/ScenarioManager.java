package net.sgcraft.uhcmorescenarios.scenarios;

import net.sgcraft.uhcmorescenarios.util.ChatUtils;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Set;

public class ScenarioManager {

    private static ScenarioManager instance;
    private HashMap<Scenario, UHCScenario> scenarios = new HashMap<Scenario, UHCScenario>();

    private ScenarioManager() {
        registerItem(Scenario.ELECTRIC_SNOWBALLS, new ElectricSnowballs());
        registerItem(Scenario.COMBO_MODE, new ComboMode());
        registerItem(Scenario.NO_DIRECT_DAMAGE, new NoDirectDamage());
        registerItem(Scenario.VAMPIRISM, new Vampirism());
        registerItem(Scenario.FISHING_WHIP, new FIshingWhip());
    }

    public Set<Scenario> getKeys() {
        return scenarios.keySet();
    }

    public void registerItem(Scenario key, UHCScenario item) {
        scenarios.put(key, item);
    }

    public UHCScenario getScenario(Scenario key) {
        return scenarios.get(key);
    }

    public UHCScenario getScenario(ItemStack item) {
        for(Scenario key : getKeys()) {
            if(ChatUtils.parseMessage(getScenario(key).getDisplayName()).equals(item.getItemMeta().getDisplayName())) {
                return scenarios.get(key);
            }
        }
        return null;
    }

    public static ScenarioManager getInstance() {
        if(instance == null) {
            instance = new ScenarioManager();
        }
        return instance;
    }
}

