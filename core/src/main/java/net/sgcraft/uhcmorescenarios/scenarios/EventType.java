package net.sgcraft.uhcmorescenarios.scenarios;

public enum EventType {
    PROJECTILELAUNCHEVENT,
    PROJECTILEHITEVENT,
    ENTITYDAMAGEBYENTITYEVENT
}
