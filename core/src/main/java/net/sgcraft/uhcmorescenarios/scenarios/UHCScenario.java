package net.sgcraft.uhcmorescenarios.scenarios;

import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public interface UHCScenario {
    public void onTrigger(Event event, EventType name);

    public Scenario getKey();
    public ArrayList<EventType> getEvents();

    public Material getMaterial();
    public String getDisplayName();
    public ArrayList<String> getDescription();

    public boolean isEnabled();
    public void setEnabled(boolean enabled);
    public ItemStack getGUIItem();
}
