package net.sgcraft.uhcmorescenarios.scenarios;

import net.sgcraft.uhcmorescenarios.util.ChatUtils;
import net.sgcraft.uhcmorescenarios.util.ItemUtils;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Vampirism implements UHCScenario {
    boolean enabled;

    public Vampirism() {
        this.enabled = false;
    }

    @Override
    public void onTrigger(Event event, EventType name) {
        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        if(!(e.getDamager() instanceof Player)) return;

        Player damager = (Player) e.getDamager();
        double health = damager.getHealth() + e.getFinalDamage();
        if(health > 20) health = 20;
        damager.setHealth(health);
        damager.playEffect(damager.getLocation(), Effect.HEART, 1);
    }

    @Override
    public Scenario getKey() {
        return Scenario.VAMPIRISM;
    }

    @Override
    public ArrayList<EventType> getEvents() {
        ArrayList<EventType> types = new ArrayList<>();
        types.add(EventType.ENTITYDAMAGEBYENTITYEVENT);
        return types;
    }

    @Override
    public Material getMaterial() {
        return Material.RED_ROSE;
    }

    @Override
    public String getDisplayName() {
        return "&4Vampirism";
    }

    @Override
    public ArrayList<String> getDescription() {
        ArrayList<String> lores = new ArrayList<String>();
        lores.add(ChatUtils.parseMessage("&fLeech life from other players and mobs"));
        return lores;
    }

    //These methods below should be the same for all scenarios
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public ItemStack getGUIItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemUtils.setName(item, getDisplayName());
        ItemUtils.setLores(item, getDescription());
        if(this.enabled) {
            item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
        } else {
            item.removeEnchantment(Enchantment.DURABILITY);
        }
        return item;
    }
}
