package net.sgcraft.uhcmorescenarios.scenarios;

public enum Scenario {
    ELECTRIC_SNOWBALLS,
    COMBO_MODE,
    NO_DIRECT_DAMAGE,
    VAMPIRISM,
    FISHING_WHIP
}
