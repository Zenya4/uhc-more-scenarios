package net.sgcraft.uhcmorescenarios.scenarios;

import net.sgcraft.uhcmorescenarios.util.ChatUtils;
import net.sgcraft.uhcmorescenarios.util.ItemUtils;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class NoDirectDamage implements UHCScenario {
    boolean enabled;

    public NoDirectDamage() {
        this.enabled = false;
    }

    @Override
    public void onTrigger(Event event, EventType name) {
        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        if(!(e.getDamager() instanceof Player) || !(e.getEntity() instanceof Player)) return;

        e.setCancelled(true);
    }

    @Override
    public Scenario getKey() {
        return Scenario.NO_DIRECT_DAMAGE;
    }

    @Override
    public ArrayList<EventType> getEvents() {
        ArrayList<EventType> types = new ArrayList<>();
        types.add(EventType.ENTITYDAMAGEBYENTITYEVENT);
        return types;
    }

    @Override
    public Material getMaterial() {
        return Material.ANVIL;
    }

    @Override
    public String getDisplayName() {
        return "&aNo Direct Damage";
    }

    @Override
    public ArrayList<String> getDescription() {
        ArrayList<String> lores = new ArrayList<String>();
        lores.add(ChatUtils.parseMessage("&fYou cannot directly damage other players"));
        lores.add(ChatUtils.parseMessage("&fBe creative and use the environment to your advantage"));
        return lores;
    }

    //These methods below should be the same for all scenarios
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public ItemStack getGUIItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemUtils.setName(item, getDisplayName());
        ItemUtils.setLores(item, getDescription());
        if(this.enabled) {
            item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
        } else {
            item.removeEnchantment(Enchantment.DURABILITY);
        }
        return item;
    }
}
