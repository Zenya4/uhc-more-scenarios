package net.sgcraft.uhcmorescenarios.scenarios;

import net.sgcraft.uhcmorescenarios.UHCMoreScenarios;
import net.sgcraft.uhcmorescenarios.util.ChatUtils;
import net.sgcraft.uhcmorescenarios.util.ItemUtils;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class ComboMode implements UHCScenario {
    boolean enabled;

    public ComboMode() {
        this.enabled = false;
    }

    @Override
    public void onTrigger(Event event, EventType name) {
        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        if(!(e.getDamager() instanceof Player) || !(e.getEntity() instanceof Player)) return;

        new BukkitRunnable() {
            @Override
            public void run() {
                ((Player) e.getEntity()).setNoDamageTicks(0);
            }
        }.runTaskLater(UHCMoreScenarios.getInstance(), 1L);
    }

    @Override
    public Scenario getKey() {
        return Scenario.COMBO_MODE;
    }

    @Override
    public ArrayList<EventType> getEvents() {
        ArrayList<EventType> types = new ArrayList<>();
        types.add(EventType.ENTITYDAMAGEBYENTITYEVENT);
        return types;
    }

    @Override
    public Material getMaterial() {
        return Material.DIAMOND_SWORD;
    }

    @Override
    public String getDisplayName() {
        return "&cCombo Mode";
    }

    @Override
    public ArrayList<String> getDescription() {
        ArrayList<String> lores = new ArrayList<String>();
        lores.add(ChatUtils.parseMessage("&fAbsolutely no invulnerability ticks when attacking players"));
        lores.add(ChatUtils.parseMessage("&fBecause 1.8 ain\'t good enough"));
        return lores;
    }

    //These methods below should be the same for all scenarios
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public ItemStack getGUIItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemUtils.setName(item, getDisplayName());
        ItemUtils.setLores(item, getDescription());
        if(this.enabled) {
            item.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
        } else {
            item.removeEnchantment(Enchantment.DURABILITY);
        }
        return item;
    }
}
