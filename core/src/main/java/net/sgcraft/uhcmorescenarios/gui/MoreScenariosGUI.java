package net.sgcraft.uhcmorescenarios.gui;

import net.sgcraft.uhcmorescenarios.scenarios.Scenario;
import net.sgcraft.uhcmorescenarios.scenarios.ScenarioManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class MoreScenariosGUI implements InventoryHolder {
    private static MoreScenariosGUI instance;
    private ScenarioManager scenarioManager = ScenarioManager.getInstance();
    private Inventory inv;

    private MoreScenariosGUI() {
        inv = Bukkit.createInventory(this, 54, ChatColor.translateAlternateColorCodes('&', "&bMore UHC Scenarios"));
        initItems();
    }

    @Override
    public Inventory getInventory() {
        return inv;
    }

    public void initItems() {
        inv.clear();

        for(Scenario key : scenarioManager.getKeys()) {
            ItemStack item = scenarioManager.getScenario(key).getGUIItem();
            inv.addItem(item);
        }
    }

    public void openInventory(Player player) {
        initItems();
        player.openInventory(inv);
    }

    public static MoreScenariosGUI getInstance() {
        if(instance == null) {
            instance = new MoreScenariosGUI();
        }
        return instance;
    }
}

