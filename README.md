# UHC More Scenarios

### Compiling
Maven is used for compiling. To compile, use the **package** option in "Run Maven" if you are using IntelliJ. Be sure to select the **parent** POM file under the root directory (**not** core or dist) 
<br>
<img src="https://imgur.com/dVP8y5Y.png">
<br>
For command line, use `mvn package` in the **root directory**. Your compiled jar will be found in `/target/UHCMoreScenarios.jar` 
<br>

### Registering Scenarios
1. Add your scenario name (key) to the enum in `scenarios.Scenario.java`
2. If applicable, add events you will be using to the enum in `scenarios.EventType.java`. 
    * The name of the EventType **must** match the name of the Spigot event, in all UPPERCASE
    * If the events you are using don't already exist, register a new event in `event.Listeners.java`. Your scenario code should **not** be in the Listeners class, as it will be inside your own class as discussed in step (3)
    * For the event registered in `event.Listeners.java`, add the following code block. `ScenarioHandler.handle(Event, EventType)`, where Event is the event itself and EventType is defined in the step above. Do **not** modify `event.ScenarioHandler.java`.
3. Create your new scenario class under the package `scenarios`, and make sure it `implements UHCScenario`. All your main event code should be written in `onTrigger()` (Refer to `scenarios.SnowballBoom.java`)
4. Register your scenario in `scenarios.ScenarioManager.java`
<br>

### Config
Set config values using `ConfigManager.getInstance()` to allow for config reloading and better error handling
<br>

### Utilities
**ChatUtils** - Use this to send colored messages instead of using `player.sendMessage()`<br>
**ItemUtils** - A utility made to simplify setting and getting colored itemstack displaynames & lores<br>
**MetaUtils** - Store, modify or read temporary metadata on players or entities
